#include "plane.h"
#include <limits.h>
#include <stdio.h>
#include <math.h>
void PlaneInit(struct Plane *plane, double sx, double sy, double sh, double FOV, double vel, double sdir, double maxDel, double baseDel)
{
	plane->x=sx;
	plane->y=sy;
	plane->height=sh;
	plane->cameraFOV=FOV;
	plane->velocity=vel;
	plane->direction=sdir;
	plane->maxDelta=maxDel;
	plane->baseDelta=baseDel;
	plane->turnRate=0;
}
void copyPlane(struct Plane p,struct Plane *p2)
{
	p2->x=p.x;
	p2->y=p.y;
	p2->height=p.height;
	p2->cameraFOV=p.cameraFOV;
	p2->velocity=p.velocity;
	p2->direction=p.direction;
	p2->maxDelta=p.maxDelta;
	p2->baseDelta=p.baseDelta;
	p2->turnRate=0;
}
void move(struct Plane* p)
{
	p->direction+=p->turnRate;
	p->x+=p->velocity*cos(p->direction);
	p->y+=p->velocity*sin(p->direction);
}
bool canTurn(struct Plane p, bool left)
{
	if(left)
	{
		return p.turnRate<p.maxDelta;
	}
	else
		return p.turnRate>(-p.maxDelta);
}
void turn(struct Plane* p, bool left)
{
	if(left)
	{
		p->turnRate=p->turnRate+p->baseDelta;//(p->baseDelta+p->turnRate<p->maxDelta)?;
	}
	else
	{
		p->turnRate-=p->baseDelta;
	}
	p->direction+=p->turnRate;
}
