#include "plane.h"
#include <limits.h>
struct Map
{
	int width;
	int height;
	short *array;
	int tilesTraversed;
	int tilesObserved;
	int totalTiles;
};
void MapInit(struct Map*, short*,int,int,int);
struct Plane movePlane(struct Map*, struct Plane, bool, bool);
double percentObserved(struct Map*);
short getTile(struct Map*, int, int);
short setTile(struct Map*, int, int, short);
void observeTiles(struct Map*, int,int,double);
void copyMap(struct Map,struct Map*);


